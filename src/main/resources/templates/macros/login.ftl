<#macro login path isRegisterForm>
    <form action="${path}" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">User name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control ${usernameError???string('is-invalid', '')}"
                       value="<#if user??>${user.username}</#if>" name="username" placeholder="Username">
                <#if usernameError??>
                    <div class="invalid-feedback">
                        ${usernameError}
                    </div>

                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-6">
                <input type="password" class="form-control ${passwordError???string('is-invalid', '')}"
                       name="password" placeholder="Password">

                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>

                </#if>
            </div>
        </div>
        <#if isRegisterForm>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Confirm password</label>
                <div class="col-sm-6">
                    <input type="password" class="form-control"
                           name="password2" placeholder="Confirm password">

                </div>
            </div>

        </#if>

        <#if isRegisterForm>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-6">
                    <input type="email" class="form-control ${emailError???string('is-invalid', '')}"
                           value="<#if user??>${user.email}</#if>" name="email" placeholder="some@some.com">

                    <#if emailError??>
                        <div class="invalid-feedback">
                            ${emailError}
                        </div>
                    </#if>
                </div>
            </div>
        </#if>
        <input type="hidden" name="_csrf" value="${_csrf.token}">
        <#if !isRegisterForm>
            <a href="/registration">Add new user</a>
        </#if>
        <div class="form-group row">
            <div class="col-sm-6">
                <button type="submit" class="btn btn-primary">
                    <#if isRegisterForm>Create<#else>Sign in</#if>
                </button>
            </div>
        </div>
    </form>
</#macro>

<#macro logout>
    <form action="/logout" method="post">
        <input type="hidden" name="_csrf" value="${_csrf.token}">
        <button class="btn btn-primary" type="submit">Sign out</button>
    </form>
</#macro>