<#include 'macros/common.ftl'>
<#include 'macros/login.ftl'>

<@page>
    <div class="mb-2">Add new user</div>
    ${message!}
    <@login "/registration" true/>
</@page>