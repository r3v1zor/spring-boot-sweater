<#include 'macros/common.ftl'>
<#include 'macros/login.ftl'>

<@page>
    <div class="mb-4">
        <h5>
            Login page
        </h5>
    </div>
    <#if Session?? && Session.SPRING_SECURITY_LAST_EXCEPTION??>
        <div class="alert alert-danger" role="alert">
            ${Session.SPRING_SECURITY_LAST_EXCEPTION.message}
        </div>
    </#if>
    <@login "/login" false/>
</@page>
