package com.example.springbootsweater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSweaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSweaterApplication.class, args);
    }

}
