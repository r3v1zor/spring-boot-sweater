package com.example.springbootsweater.controllers;

import com.example.springbootsweater.domain.Message;
import com.example.springbootsweater.domain.User;
import com.example.springbootsweater.repos.MessageRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class MainController {
    private MessageRepository messageRepository;

    @Value("${upload.path}")
    private String uploadPath;

    public MainController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping
    public String greeting() {
        return "greeting";
    }

    @GetMapping("/main")
    public String main(@RequestParam(required = false, defaultValue = "") String filter, Model model) {
        List<Message> messages;

        if (filter != null && !filter.isEmpty()) {
            messages = messageRepository.findByTag(filter);
        } else {
            messages = messageRepository.findAll();
        }

        model.addAttribute("messages", messages);
        model.addAttribute("filter", filter);
        return "main";
    }

    @PostMapping("/main")
    public String addMessage(@AuthenticationPrincipal User user,
                             @Valid Message message,
                             BindingResult bindingResult,
                             @RequestParam("file") MultipartFile file,
                             Model model) throws IOException {

        if (message != null) {

            message.setAuthor(user);

            if (bindingResult.hasErrors()) {

                final Map<String, String> errorsMap = UtilsController.getErrors(bindingResult);
                model.mergeAttributes(errorsMap);
                model.addAttribute("message", message);

            } else {


                if (file != null && !file.getOriginalFilename().isEmpty()) {
                    final File uploadDir = new File(uploadPath);

                    if (!uploadDir.exists()) {
                        uploadDir.mkdir();
                    }

                    final String filename = UUID.randomUUID().toString();
                    final String resultName = filename + "." + file.getOriginalFilename();
                    message.setFilename(resultName);

                    file.transferTo(new File(uploadPath + "/" + resultName));
                }

                messageRepository.save(message);
                model.addAttribute("message", null);
            }



            final List<Message> messages = messageRepository.findAll();
            model.addAttribute("messages", messages);
        }

        return "main";
    }



    @PostMapping("filter")
    public String filterByTag(@RequestParam String tag, Map<String, Object> model) {
        if (tag != null) {
            final List<Message> messages;

            if (!tag.isEmpty()) {
                messages = messageRepository.findByTag(tag);
            } else {
                messages = messageRepository.findAll();
            }

            model.put("messages", messages);
        }

        return "main";
    }
}
