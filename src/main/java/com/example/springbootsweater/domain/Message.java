package com.example.springbootsweater.domain;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "Please, fill the message")
    @Length(max = 2048, message = "Message to long (more than 2048 symbols)")
    private String text;
    @NotBlank(message = "Please, fill the tag")
    @Length(max = 255, message = "Tag too long (more than 255 symbols)")
    private String tag;

    private String filename;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;

    public Message() {
    }

    public Message(String text, String tag, User author) {
        this.text = text;
        this.tag = tag;
        this.author = author;
    }

    public String authorName() {
        return author != null ? author.getUsername() : "<none>";
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
