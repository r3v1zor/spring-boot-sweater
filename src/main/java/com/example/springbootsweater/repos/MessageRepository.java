package com.example.springbootsweater.repos;

import com.example.springbootsweater.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findAll();

    List<Message> findByTag(String tag);
}
